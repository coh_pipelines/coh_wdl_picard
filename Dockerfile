ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_openjdk:8u as opt_openjdk

RUN mkdir -p /opt/picard/ && \
    wget -t 3 https://github.com/broadinstitute/picard/releases/download/2.22.0/picard.jar -O /opt/picard/picard.jar
